public class MetaData {
    private final ResultSet resultset;

    public ResultSet getResultSet() { return resultset; }

    public MetaData(ResultSet rs){ resultset = rs; }

    public int getCount() { return resultset.getCount(); }
    public int getLimit() { return resultset.getLimit(); }
    public int getOffset() { return resultset.getOffset(); }
}
