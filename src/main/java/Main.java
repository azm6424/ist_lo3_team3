import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;

public class Main {

    public static void main(String [] args) {
        String token = "MpHrFKpGClZKvyzCXNrAFHUmATUpZrFm";
        Gson gson = new Gson();
        String response;
        HttpURLConnection conn = null;

        try {
            URL url = new URL("https://www.ncdc.noaa.gov/cdo-web/api/v2/datasets");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("token", token);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            conn.getInputStream()
                    )
            );

            response = br.readLine();
            NoaaData data = gson.fromJson(response, NoaaData.class);

            System.out.println("########## Result Set ##########");
            System.out.printf("%-15s%s\n", "Offset", data.getOffset());
            System.out.printf("%-15s%s\n", "Count", data.getCount());
            System.out.printf("%-15s%s\n", "Limit", data.getLimit());
            System.out.println();

            // TODO print out results

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert conn != null;    // paranoia
            conn.disconnect();
        }
        conn.disconnect();
    }
}
