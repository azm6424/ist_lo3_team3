public class ResultSet {
    private int offset;
    private int count;
    private int limit;
    public ResultSet() {};

    public int getOffset() { return offset; }
    public int getCount() { return count; }
    public int getLimit() { return limit; }
}
