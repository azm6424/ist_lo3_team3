public class NoaaData {
    private MetaData metadata;
    public NoaaData() {};

    public MetaData getMetadata() { return metadata; }
    public int getCount() { return metadata.getCount(); }
    public int getLimit() { return metadata.getLimit(); }
    public int getOffset() { return metadata.getOffset(); }
}
